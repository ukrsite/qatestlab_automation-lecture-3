package myprojects.automation.assignment3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

import java.util.Arrays;
import java.util.stream.Collectors;

public class EventHandler implements WebDriverEventListener {

    @Override
    public void beforeNavigateTo(String s, WebDriver webDriver) {
        System.out.println("Open url: " + s);
    }

    @Override
    public void afterNavigateTo(String s, WebDriver webDriver) {

        System.out.println("Url successfully opened ");
        System.out.println("The current page title is: " + webDriver.getTitle());
    }

    @Override
    public void beforeNavigateBack(WebDriver webDriver) { System.out.println("Navigate back");  }

    @Override
    public void afterNavigateBack(WebDriver webDriver) {
        System.out.println("Current url: " + webDriver.getCurrentUrl());
    }

    @Override
    public void beforeNavigateForward(WebDriver webDriver) { System.out.println("Navigate forward");  }

    @Override
    public void afterNavigateForward(WebDriver webDriver) {
        System.out.println("Current url: " + webDriver.getCurrentUrl());
    }

    @Override
    public void beforeNavigateRefresh(WebDriver webDriver) { System.out.println("Page refresh"); }

    @Override
    public void afterNavigateRefresh(WebDriver webDriver) {
        System.out.println("Current url: " + webDriver.getCurrentUrl());
    }

    @Override
    public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {
        System.out.println("Search for element: " + by.toString());
    }

    @Override
    public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {
        System.out.println("Element found successfully");
    }

    @Override
    public void beforeClickOn(WebElement webElement, WebDriver webDriver) {
        System.out.println("Click on element: " + webElement.getTagName() + " " + webElement.getAttribute("name")
                + " " + webElement.getAttribute("id"));
    }

    @Override
    public void afterClickOn(WebElement webElement, WebDriver webDriver) { }

    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        String value = Arrays.stream(charSequences).map(CharSequence::toString).collect(Collectors.joining());
        System.out.print(String.format("Change value of %s: %s\n", webElement.getTagName(), value));
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {

    }

    @Override
    public void beforeScript(String s, WebDriver webDriver) {
        System.out.println("Execute script: " + s);
    }

    @Override
    public void afterScript(String s, WebDriver webDriver) {
        System.out.println("Script executed");
    }

    @Override
    public void onException(Throwable throwable, WebDriver webDriver) {

    }
}
