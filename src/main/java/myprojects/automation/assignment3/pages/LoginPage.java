package myprojects.automation.assignment3.pages;

        import myprojects.automation.assignment3.GeneralActions;
        import myprojects.automation.assignment3.utils.Properties;
        import org.openqa.selenium.By;
        import org.openqa.selenium.support.events.EventFiringWebDriver;
        import org.openqa.selenium.support.ui.ExpectedConditions;
        import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private EventFiringWebDriver driver;
    private By emailInput = By.id("email");
    private By passwordInput = By.id("passwd");
    private By loginBtn = By.name("submitLogin");

    public LoginPage(EventFiringWebDriver driver){
        this.driver = driver;
    }

    public void open(){
        driver.get(Properties.getBaseAdminUrl());
    }

    public void fillEmailInput(String login){
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.presenceOfElementLocated(emailInput)).sendKeys(login);
    }

    public void fillPasswordInput(String password){
        driver.findElement(passwordInput).sendKeys(password);
    }

    public void clickLoginButton(){
        driver.findElement(loginBtn).click();
    }
}


