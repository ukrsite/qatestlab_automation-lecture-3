package myprojects.automation.assignment3.pages;

        import org.openqa.selenium.By;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.interactions.Actions;
        import org.openqa.selenium.support.events.EventFiringWebDriver;
        import org.openqa.selenium.support.ui.ExpectedConditions;
        import org.openqa.selenium.support.ui.WebDriverWait;

public class DashboardPage {
    private static final String alertMessageCheck = "Создано";
    private EventFiringWebDriver driver;
    private By logoutImage = By.cssSelector("span.employee_avatar_small");
    private By logoutButton = By.id("header_logout");
    private By catalogTab = By.id("subtab-AdminCatalog");
    private By subMenuCategory = By.cssSelector("ul.submenu");
    private By newCategoryButton = By.id("page-header-desc-category-new_category");
    private By inputNewCategory = By.id("name_1");
    private By categorySubmitButton = By.id("category_form_submit_btn");
    private By alertSuccess = By.cssSelector("div.alert.alert-success");
    private By backDescCategoryButton = By.id("desc-category-back");
    private By inputCategoryFilterName = By.name("categoryFilter_name");
    private By checkInputCategoryFilter = By.cssSelector("td[class=pointer]");
    private By categoryFilterQuantity = By.cssSelector("span.badge");

    public DashboardPage(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public void clickLogoutImage() {
        driver.findElement(logoutImage).click();
    }

    public void clickLogoutButton() {
        driver.findElement(logoutButton).click();
    }

    public void selectCatalogItem() {
        WebElement catalogTabElement = driver.findElement(catalogTab);
        Actions actions = new Actions(driver);
        actions.moveToElement(catalogTabElement).build().perform();

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(subMenuCategory));

        catalogTabElement.findElements(By.cssSelector("li")).get(1).click();
    }

    public void clickAddCategoryButton() {
        driver.findElement(newCategoryButton).click();
    }

    public void fillNewCategoryItem(String newCategoryItem) {
        driver.findElement(inputNewCategory).sendKeys(newCategoryItem);
    }

    public void submitNewCategoryItem() {
        driver.findElement(categorySubmitButton).click();
    }

    public void checkAlertSuccess() {
        String alertMessage = driver.findElement(alertSuccess).getText();
        if (alertMessage.endsWith(alertMessageCheck)) {
            System.out.println("The new category created successfully");
        } else {
            System.out.println("The new category not created");
        }
    }

    public void clickBackDescCategoryButton() {
        driver.findElement(backDescCategoryButton).click();
    }

    public void fillCategoryFilterName(String newCategory) {
        driver.findElement(inputCategoryFilterName).sendKeys(newCategory);
    }

    public void submitCategoryFilterName() {
        driver.findElement(inputCategoryFilterName).submit();
    }

    public void checkCategoryFilter(String newCategory) {
        String newCategoryCheck = driver.findElement(checkInputCategoryFilter).getText();
        if (newCategory.equals(newCategoryCheck)) {
            System.out.println("The category found successfully");
            System.out.println("The quantity of a category is : " +
                    Integer.valueOf(driver.findElement(categoryFilterQuantity).getText()));
        } else {
            System.out.println("The category not found");
        }
    }
}

