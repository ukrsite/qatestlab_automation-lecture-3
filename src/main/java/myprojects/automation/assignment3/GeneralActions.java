package myprojects.automation.assignment3;

import myprojects.automation.assignment3.pages.DashboardPage;
import myprojects.automation.assignment3.pages.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By logoutImage = By.cssSelector("span.employee_avatar_small");

    public GeneralActions(EventFiringWebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        // TODO implement logging in to Admin Panel
        LoginPage loginPage = new LoginPage(driver);

        loginPage.open();

        loginPage.fillEmailInput(login);
        loginPage.fillPasswordInput(password);
        loginPage.clickLoginButton();
    }

    /**
     * Adds new category in Admin Panel.
     * @param categoryName
     */
    public void createCategory(String categoryName) {
        // TODO implement logic for new category creation
        DashboardPage dashboardPage = new DashboardPage(driver);

        waitForContentLoad();
        dashboardPage.selectCatalogItem();
        waitForContentLoad();
        dashboardPage.clickAddCategoryButton();
        waitForContentLoad();
        dashboardPage.fillNewCategoryItem(categoryName);
        dashboardPage.submitNewCategoryItem();
        waitForContentLoad();
        dashboardPage.checkAlertSuccess();
        dashboardPage.clickBackDescCategoryButton();
        waitForContentLoad();
        dashboardPage.fillCategoryFilterName(categoryName);
        dashboardPage.submitCategoryFilterName();
        waitForContentLoad();
        dashboardPage.checkCategoryFilter(categoryName);
        dashboardPage.clickLogoutImage();
        dashboardPage.clickLogoutButton();
    }

    /**
     * Waits until page loader disappears from the page
     */
    private void waitForContentLoad() {
        // TODO implement generic method to wait until page content is loaded
        wait.until(ExpectedConditions. elementToBeClickable(logoutImage));
    }

}
