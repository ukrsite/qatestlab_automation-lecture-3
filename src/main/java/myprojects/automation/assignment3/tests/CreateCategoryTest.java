package myprojects.automation.assignment3.tests;

import myprojects.automation.assignment3.BaseScript;
import myprojects.automation.assignment3.GeneralActions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class CreateCategoryTest extends BaseScript {
    public static void main(String[] args) throws InterruptedException {
        final String LOGIN = "webinar.test@gmail.com";
        final String PASSWORD = "Xcg7299bnSmMuRLp9ITw";
        final String CATEGORY_NAME = "newCategory";

        EventFiringWebDriver driver = getConfiguredDriver();
        GeneralActions generalActions = new GeneralActions(driver);

        // login
        generalActions.login(LOGIN, PASSWORD);
        // create category, check that new category appears in Categories table
        generalActions.createCategory(CATEGORY_NAME);
        // finish script
        quitDriver(driver);
    }
}
